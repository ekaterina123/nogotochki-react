import React, {Component} from 'react';
import './bootstrap.css';

class MyNavbar extends Component {
    render() {
        return (<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <a className="navbar-brand" href="#">
                    Ноготочки
                    <img src="images/logo.png" height="50" width="50"/>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="index.html">Главная</a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                Услуги
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item " href="manic.html">Маникюр</a>
                                <a className="dropdown-item" href="pedic.html">Педикюр</a>
                                <a className="dropdown-item" href="depil.html">Депиляция</a>
                                <a className="dropdown-item" href="makeup.html">Макияж</a>
                            </div>
                        </li>
                    </ul>
                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="search" placeholder="Поиск услуг"
                               aria-label="Search"
                               id="Poisk"/>
                        <button className="btn btn-outline-danger my-2 my-sm-0" type="submit" id="searchBtn">Поиск
                        </button>
                    </form>
                    <div className="btn-group ml-2">
                        <button type="button" className="btn btn-danger dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            Имя пользователя
                        </button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#" data-toggle="modal"
                               data-target="#exampleModal1">Войти</a>
                            <button className="dropdown-item" data-toggle="modal" data-target="#exampleModal"
                                    id="btnZap">
                                Записаться на услуги
                            </button>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#">Выход</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>)
    }
}

export default MyNavbar;