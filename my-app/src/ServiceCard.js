import React, {Component} from 'react';
import './bootstrap.css';

class ServiceCard extends Component {
    constructor(props) {
        super(props);
        //serviceId, title, image, desc
        this.state = {
            isAdded : false,
        }
    }

    addButtonHandle = () => {
        this.props.addOrderButtonHandle(this.props.serviceId);
        this.setState({isAdded:true});
    };

    delButtonHandle = () => {
        this.props.delOrderButtonHandle(this.props.serviceId);
        this.setState({isAdded:false});
    }

    renderAddDelBtn() {
      if (!this.state.isAdded)
          return (<button className="btn btn-primary btnAdd" data-service-id={this.props.serviceId}
                          onClick={this.addButtonHandle}>Добавить в корзину</button>)
        else
            return (<button className="btn btn-danger btnAdd" data-service-id={this.props.serviceId}
                            onClick={this.delButtonHandle}>Удалить из корзины</button>);
    }

    render() {
        return (
            <div className="card m-2 ServiceCard hands" data-hands="true">
                <img className="card-img-top" src={this.props.imagePath} alt="Card image cap"/>
                <div className="card-body">
                    <h5 className="card-title" id="Add1">{this.props.title}</h5>
                    <p className="card-text">{this.props.desc}</p>
                    {this.renderAddDelBtn()}
                </div>
            </div>
        );
    }
}

export default ServiceCard;