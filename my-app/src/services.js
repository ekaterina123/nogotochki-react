import React, {Component} from 'react';
import './bootstrap.css';

import ServiceCard from './ServiceCard'
import MyNavbar from "./MyNavbar";
import ServiceCheckBox from "./ServiceCheckBox"

class Services extends Component {
    constructor(props) {
        super(props);
        console.log('hello world!');
        this.state = {
            showFace: true,
            showLegs: false,
            showHands: true,
            orders: [], //chosen items
        };
    }

    handleCheckChange = (event) => {
        console.log(event.currentTarget.id);
        let id = event.currentTarget.id;
        switch (id) {
            case 'faceCheck':
                this.setState({
                        showFace: !this.state.showFace
                    }
                )
                console.log('yo')
                ;
                break;
            case 'legsCheck':
                this.setState({
                        showLegs: !this.state.showLegs
                    }
                );
                break;
            case 'handsCheck':
                this.setState({
                    showHands: !this.state.showHands
                });
                break;
        }
    };

    addService = (serviceId) => {
        let newOrders = this.state.orders;
        newOrders.push(serviceId);
        this.setState({orders: newOrders});
    };
    delService = (serviceId) => {
        let newOrders = this.state.orders.filter(value => value != serviceId);
        this.setState({orders: newOrders});
    };

    renderServiceCards() {
        let showFace = this.state.showFace;
        let showLegs = this.state.showLegs;
        let showHands = this.state.showHands;
        return <div class="row justify-content-center">

            {showHands ? (<ServiceCard serviceId={'manic'} title={'Маникюр'} desc={'Очень хороший маникюр'}
                                       imagePath={"images/main/a1.png"} addOrderButtonHandle={this.addService}
                                       delOrderButtonHandle={this.delService}/>) : ''}
            {showLegs ? <ServiceCard serviceId={'pedic'} title={'Педикюр'} desc={'Очень хороший педикюр'}
                                     imagePath={"images/main/a2.png"} addOrderButtonHandle={this.addService}
                                     delOrderButtonHandle={this.delService}/> : ''}
            {showHands || showLegs ?
                <ServiceCard serviceId={'depil'} title={'Депиляция'} desc={'Очень хорошая депиляция'}
                             imagePath={"images/main/a3.png"} addOrderButtonHandle={this.addService}
                             delOrderButtonHandle={this.delService}/> : ''}
            {showFace ?
                <ServiceCard serviceId={'makeup'} title={'Макияж'} desc={'Очень хороший макияж, даже не придется пить'}
                             imagePath={"images/main/a4.png"} addOrderButtonHandle={this.addService}
                             delOrderButtonHandle={this.delService}/> : ''}
            {showFace ? <ServiceCard serviceId={'coloring'} title={'Окрашивание'} desc={'Как получится'}
                                     imagePath={"images/main/a5.png"} addOrderButtonHandle={this.addService}
                                     delOrderButtonHandle={this.delService}/> : ''}
            {showFace ? <ServiceCard serviceId={'haircut'} title={'Стрижка'} desc={'Не советую'}
                                     imagePath={"images/main/a6.png"} addOrderButtonHandle={this.addService}
                                     delOrderButtonHandle={this.delService}/> : ''}
        </div>
    }

    renderOrders = () => {
        let ordersNames = {
            'manic': 'Маникюр',
            'pedic': 'Педикюр',
            'depil': 'Депиляция',
            'makeup': 'Макияж',
            'coloring': 'Окрашивание',
            'haircut': 'Стрижка'
        };
        let output = this.state.orders.map((value =>
            <li className="list-group-item">{ordersNames[value]}</li>));
        if (output.length==0)
            output = 'Пусто';
        return (
            <div>
                <label htmlFor="ordersList">Список заказов</label>
                <ul class="list-group" id='ordersList'>
                    {output}
                </ul>
            </div>
        )
    }

    render() {
        return (
            <div>
                <MyNavbar/>
                <div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1 col-sm-12">
                                <div class="form-group">
                                    <ServiceCheckBox checked={this.state.showFace} label={'Лицо'} checkId={'faceCheck'}
                                                     handleCheckChange={this.handleCheckChange}/>
                                    <ServiceCheckBox checked={this.state.showHands} label={'Руки'}
                                                     checkId={'handsCheck'}
                                                     handleCheckChange={this.handleCheckChange}/>
                                    <ServiceCheckBox checked={this.state.showLegs} label={'Ноги'} checkId={'legsCheck'}
                                                     handleCheckChange={this.handleCheckChange}/>
                                </div>
                                <div class="row">
                                    {this.renderOrders()}
                                </div>
                            </div>
                            <div class="col">
                                {this.renderServiceCards()}
                            </div>
                        </div>


                    </div>
                </div>
                <button id="BtnZv">Заказать обратный звонок</button>
            </div>

        );
    }
}

export default Services;