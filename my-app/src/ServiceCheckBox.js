import React, {Component} from 'react';
import './bootstrap.css';


class ServiceCheckBox extends Component {
    render() {
        //checkId, handleCheckChange, checked, label
        return (
        <div className="form-check">
            <input className="form-check-input ServicesFilterCheck" type="checkbox"
                   checked={this.props.checked}
                   id={this.props.checkId} onChange={this.props.handleCheckChange}/>
            <label className="form-check-label" htmlFor="faceCheck">
                {this.props.label}
            </label>
        </div>)
    }

}

export default ServiceCheckBox;