import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Test from './Test';
import Services from './services'


ReactDOM.render(<Services />, document.getElementById('root'));
registerServiceWorker();

